import json
from urlparse import parse_qs
from pond5 import Bot
import random

def handler(event, context):

    with open('.context', 'r') as f:
        gordon_context = json.loads(f.read())

    expected_token = gordon_context['token']

    req_body = event['body']
    params = parse_qs(req_body)

    # Check if the token is the correct one
    token = params['token'][0]
    if token != expected_token:
        raise Exception("Invalid request token")

    user = params['user_name'][0]
    command = params['command'][0]
    channel = params['channel_name'][0]
    command_text = params['text'][0]

    bot = Bot.Bot()

    image_url = bot.get_image_url(command_text)

    if image_url != None:

        response = {
            'response_type': 'in_channel',
            "attachments": [
                {
                    "fallback": "an inspiration to us all!",
                    "image_url": image_url,
		    "text" : quote()
                }
            ]
        }

    else:

        response_text = "[%s] Too specific dude, be move vague. Also check your spelling!" % command_text

        response = {
            'response_type': 'ephemeral',
            'text': response_text
        }

    return response

def quote():

    quotes = [ "Look, I like making cherry product, but let's keep it real, alright? We make poison for people who don't care. We probably have the most unpicky customers in the world",
    "Yeah. Totally Kafkaesque.",
    "You don't need a criminal lawyer. You need a _criminal_ lawyer",
    "Oh well, heil Hitler, bitch. And let me tell you something else. We flipped a coin, okay? You and me. You and me! Coin flip is sacred! Your job is waiting for you in that basement, as per the coin!",
    "You either run from things, or you face them, Mr. White.",
    "Like I came to you, begging to cook meth. 'Oh, hey, nerdiest old dude I know, you wanna come cook crystal?' Please. I'd ask my diaper-wearing granny, but her wheelchair wouldn't fit in the RV.",
    "Yeah Mr. White! You really do have a plan! Yeah science!",
    "You know what? Why I'm here in the first place? Is to sell you meth. You're nothing to me but customers! I made you my bitch. You okay with that?",
    "Possum. Big, freaky, lookin' bitch. Since when did they change it to opossum? When I was comin' up it was just possum. Opossum makes it sound like he's irish or something. Why do they gotta go changing everything?",
    "So no matter what I do, hooray for me because I'm a great guy? It's all good? No matter how many dogs I kill, I just do an inventory and accept?",
    "I'm supposed to promise, cross my heart, to like straighten up and fly right, or toe the line, or some other crap I'm not going to say?",
    "I uh... I eat a lot of frozen stuff... It's usually pretty bad, I mean the pictures are always so awesome, you know? It's like ''hell yeah, I'm starved for this lasagna!'' and then you nuke it and the cheese gets all scabby on top and it's like... it's like you're eating a scab... I mean, seriously, what's that about? It's like ''Yo! What ever happened to truth in advertising?'' You know?",
    "Look... look, you two guys are just... guys, okay? Mr. White... he's the devil. You know, he is... he is smarter than you, he is luckier than you. Whatever... whatever you think is supposed to happen... I'm telling you, the exact reverse opposite of that is gonna happen, okay?",
    "Are we in the meth business, or the money business?",
    "Nah come on... man, some straight like you giant stick up his ass all of a sudden at age what 60 he's just going to break bad?",
    "What if this is like math, or algebra? And you add a plus douchebag to a minus douchebag, and you get, like, zero douchebags?",
    "Hey, you girls want to meet my fat stack?",
    "You can't admit, just for once, that I'm right. Come on. That O'Keeffe lady kept trying over and over until that stupid door was perfect.",
    "I got two dudes that turned into raspberry slushie then flushed down my toilet. I can't even take a proper dump in there. I mean, the whole damn house has got to be haunted by now.",
    "Right on. New Zealand. That's where they made Lord of the Rings. I say we just move there, yo. I mean, you can do your art, right? Like, you can paint the local castles and shit. And I can be a bush pilot.",
    "What good is being an outlaw when you have responsibilities?",
    "I'M A BLOWFISH! BLOWFISH! YEEEAAAH! BLOWFISHIN' THIS UP!",
    "You got me riding shotgun to every dark anal recess of this state. It'd be nice if you clued me in a little.",
    "Yeah, bitch! Magnets!",
    "Yo 148, 3-to-the-3-to-the-6-to-the-9. Representin' the ABQ. What up, biatch? Leave it at the tone!"
    ]

    return random.choice(quotes)

