
from lxml import html
import requests
import re
import urllib
import random
import json

class Bot:

    # Returns None if no matches found

    def get_image_url(self, search_string):

        url = self.search_string_to_url(search_string)
        return self.scrape_pond5(url)

    def search_string_to_url(self, search_string):

        # Replace runs of spaces
        search_string = re.sub(r"\s+", '-', search_string)

        # Lower case
        search_string = search_string.lower()

        # URI encode
        search_string = urllib.quote(search_string, safe='')

        return "https://www.pond5.com/photos/1/%s.html" % search_string


    def scrape_pond5(self, url):

        page = requests.get(url)
        tree = html.fromstring(page.content)

        results = tree.xpath("//div[contains(@class, 'SearchResultsV3-item')]/a/@data-item-detail")

        if len(results) == 0:
            return None

        # Trim to the first 5 results
        if len(results) > 5:
            results = results[:5]

        result = random.choice(results)
        data = json.loads(result)

        return data['downloadStillUrl']        
